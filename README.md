Cplugin
=======
Core of content plugins

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist booba2003/yii2portal-cplugin "*"
```

or add

```
"booba2003/yii2portal-cplugin": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \yii2portal\cplugin\AutoloadExample::widget(); ?>```