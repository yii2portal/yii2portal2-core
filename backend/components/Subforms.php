<?php


namespace yii2portal\core\backend\components;

use Yii;


class Subforms extends Component
{

    public function render($model, $activeForm)
    {
        return parent::render('subforms', [
            'model'=>$model,
            'activeForm'=>$activeForm,
        ]);
    }
}
