<?php


namespace backend\components;

use Yii;
use yii\base\Component;


class Title extends Component
{
    public $titles = [];

    public function get($action = null, $controller = null)
    {
        $return = null;
        $route = explode("/",Yii::$app->requestedRoute);

        if (!$action) {
            $action = array_pop($route);
        }else{
            array_pop($route);
        }
        if (!$controller) {
            $controller = array_pop($route);
        }else{
            array_pop($route);
        }


        if (isset($this->titles[$controller]) && isset($this->titles[$controller][$action])) {
            $return = $this->titles[$controller][$action];
        }
        return $return;
    }


    public function controller($controller = null)
    {
        return $this->get('index', $controller);
    }

}
