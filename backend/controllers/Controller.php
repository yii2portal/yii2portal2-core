<?php

namespace yii2portal\core\backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


abstract class Controller extends \yii2portal\core\common\controllers\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * @param $value
     * @param string $key error|danger|success|info|warning
     * @param bool $removeAfterAccess
     */
    public function setFlash($value, $key = 'info', $removeAfterAccess = true)
    {
        return Yii::$app->session->setFlash($key, $value, $removeAfterAccess);
    }
}
