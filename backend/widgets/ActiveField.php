<?php

namespace yii2portal\core\backend\widgets;


use yii\bootstrap\Html;

class ActiveField extends \yii\bootstrap\ActiveField
{

    public function dualListBox($items, $options = [])
    {

        $options = array_merge($this->inputOptions, $options);
        if(!isset($options['items'])){
            $options['items'] = $items;
        }

        if(isset($options['options'])){
            $this->addAriaAttributes($options['options']);
            $this->adjustLabelFor($options['options']);
        }


        return $this->widget(DualListbox::className(), $options);
    }

}