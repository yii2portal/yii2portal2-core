<?php

namespace yii2portal\core\backend\widgets\grid;


use yii\base\Model;
use yii\bootstrap\Html;
use yii\helpers\BaseHtml;

class DataColumn extends \yii\grid\DataColumn
{

    public $filterInputOptions = ['class' => 'form-control boot-multiselect', 'id' => null, 'multiple' => 'multiple'];

    protected function renderFilterCellContent()
    {
        if (is_string($this->filter)) {
            return $this->filter;
        }

        $model = $this->grid->filterModel;

        if ($this->filter !== false && $model instanceof Model && $this->attribute !== null && $model->isAttributeActive($this->attribute)) {
            if ($model->hasErrors($this->attribute)) {
                Html::addCssClass($this->filterOptions, 'has-error');
                $error = ' ' . Html::error($model, $this->attribute, $this->grid->filterErrorOptions);
            } else {
                $error = '';
            }
            if (is_array($this->filter)) {
                if (empty($this->filterInputOptions['multiple'])) {
                    $options = array_merge(['prompt' => ''], $this->filterInputOptions);
                } else {
                    $options = $this->filterInputOptions;
                }
                return Html::activeDropDownList($model, $this->attribute, $this->filter, $options) . $error;
            } else {
                return Html::activeTextInput($model, $this->attribute, $this->filterInputOptions) . $error;
            }
        } else {
            return parent::renderFilterCellContent();
        }
    }
}