<?php

namespace yii2portal\core\backend\widgets\grid;


use yii\bootstrap\Html;

class CheckboxColumn extends \yii\grid\CheckboxColumn
{

    public $contentOptions = [
        'class'=>'checkbox-col'
    ];
    public $headerOptions = [
        'class'=>'checkbox-col'
    ];

    protected function renderDataCellContent($model, $key, $index)
    {


        if ($this->checkboxOptions instanceof \Closure) {
            $options = call_user_func($this->checkboxOptions, $model, $key, $index, $this);
        } else {
            $options = $this->checkboxOptions;
            if (!isset($options['value'])) {
                $options['value'] = is_array($key) ? json_encode($key, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) : $key;
            }
        }

        return "<div class='checkbox'><label>" . Html::checkbox($this->name, !empty($options['checked']), $options) . "<span class=\"ico\"></span></label></div>";
    }


    protected function renderHeaderCellContent()
    {
        $name = $this->name;
        if (substr_compare($name, '[]', -2, 2) === 0) {
            $name = substr($name, 0, -2);
        }
        if (substr_compare($name, ']', -1, 1) === 0) {
            $name = substr($name, 0, -1) . '_all]';
        } else {
            $name .= '_all';
        }

        $id = $this->grid->options['id'];
        $options = json_encode([
            'name' => $this->name,
            'multiple' => $this->multiple,
            'checkAll' => $name,
        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->grid->getView()->registerJs("jQuery('#$id').yiiGridView('setSelectionColumn', $options);");

        if ($this->header !== null || !$this->multiple) {
            return parent::renderHeaderCellContent();
        } else {
            return "<div class='checkbox'><label>" . Html::checkBox($name, false, ['class' => 'select-on-check-all']) . "<span class=\"ico\"></span></label></div>";
        }
    }
}