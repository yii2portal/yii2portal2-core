<?php

namespace yii2portal\core\backend\widgets\grid;


use yii\bootstrap\Html;

class GridView extends \yii\grid\GridView
{
    public $tableOptions = ['class' => 'table table-hover'];
    public $dataColumnClass = 'yii2portal\core\backend\widgets\grid\DataColumn';


    /*public function renderTableHeader()
    {
        $cells = [];
        foreach ($this->columns as $column) {
            $cells[] = $column->renderHeaderCell();
        }
//        $content = Html::tag('tr', implode('', $cells), $this->headerRowOptions);
        $content = $this->renderFilters();

        return "<thead>\n" . $content . "\n</thead>";
    }*/
}