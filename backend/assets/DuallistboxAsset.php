<?php

namespace yii2portal\core\backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class DuallistboxAsset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-duallistbox/dist';
    public $css = [
        'bootstrap-duallistbox.min.css',
    ];
    public $js = [
        "jquery.bootstrap-duallistbox.min.js"
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
