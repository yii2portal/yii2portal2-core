<?php
namespace yii2portal\core\backend\forms;

use Yii;
use yii\db\BaseActiveRecord;

trait SubformTrait
{
    public static $subforms = [];

    private $_inited = [];
    private $_isInitedSubforms = [];

    public function initSubforms()
    {
        $this->_isInitedSubforms = true;
        foreach (self::$subforms as $subformClass) {

            $subform = Yii::createObject([
                'class' => $subformClass,
                'parent' => $this
            ]);
            if ($subform->hasPermissions()) {
                $this->_inited[$subformClass] = $subform;
                $this->_inited[$subformClass]->loadDefault();
            }
        }

    }

    public function init()
    {
        parent::init();
        $this->on(Model::EVENT_BEFORE_VALIDATE, function ($event) {
            $event->isValid = $this->validateSubforms();
        });
        $this->on(BaseActiveRecord::EVENT_BEFORE_DELETE, function ($event) {
            if (!$this->_isInitedSubforms) {
                $this->initSubforms();
            }
            $event->isValid = $this->deleteSubforms();
        });
        return $result;
    }

    public function load($data, $formName = null)
    {
        $result = parent::load($data, $formName);
        if ($result) {
            $result = $this->loadSubforms($data, $formName);
        }

        return $result;
    }

    public function loadSubforms($data, $formName = null)
    {
        $loaded = true;
        foreach ($this->getSubForms() as $subform) {
            if (!$subform->load($data, $formName)) {
                $loaded = false;
            }
        }
        return $loaded;
    }

    public function getSubForms()
    {
        return $this->_inited;
    }

    public function validateSubforms($attributeNames = null, $clearErrors = true)
    {
        $valid = true;
        foreach ($this->getSubForms() as $subform) {
            if (!$subform->validate($attributeNames, $clearErrors)) {
                $valid = false;
            }
        }
        return $valid;
    }

    public function saveSubforms($runValidation = true, $attributeNames = null)
    {
        $isValid = true;
        $return = true;
        if ($runValidation) {
            $isValid = $this->validateSubforms($attributeNames);
        }
        if ($isValid) {
            foreach ($this->getSubForms() as $subform) {
                if (!$subform->save(false, $attributeNames)) {
                    $return = false;
                }
            }
        } else {
            $return = false;
        }
        return $return;
    }

    public function deleteSubforms()
    {
        $isValid = true;

        foreach ($this->getSubForms() as $subform) {
            if (!$subform->delete()) {
                $isValid = false;
            }
        }

        return $isValid;
    }
}