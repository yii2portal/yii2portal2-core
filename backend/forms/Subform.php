<?php
namespace yii2portal\core\backend\forms;

use Yii;
use yii\base\ViewContextInterface;

abstract class Subform extends Model implements ViewContextInterface
{
    private $_view;
    public $parent;
    public $form;

    abstract public function subformId();

    abstract public function subformName();

    abstract public function loadDefault();

    abstract public function save($runValidation = true, $attributeNames = null);
    abstract public function delete();

    public function hasPermissions(){
        return true;
    }

    public function getView()
    {
        if ($this->_view === null) {
            $this->_view = Yii::$app->getView();
        }

        return $this->_view;
    }

    public function render($form, $view='default', $params = [])
    {
        $this->form = $form;
        return $this->getView()->render($view, $params, $this);
    }

    public function renderFile($form, $file, $params = [])
    {
        $this->form = $form;
        return $this->getView()->renderFile($file, $params, $this);
    }

    public function getViewPath()
    {
        $class = new \ReflectionClass($this);
        list(, $module) = explode('\\', $class->getNamespaceName());

        $viewPath = implode(DIRECTORY_SEPARATOR,[
            Yii::$app->getModule($module)->viewPath,
            "subforms",
            $class->getShortName()
        ]);
        if(is_dir($viewPath)){
            return $viewPath;
        }else{
            return dirname($class->getFileName()) . DIRECTORY_SEPARATOR . 'views';
        }
    }
}
