<?php

namespace yii2portal\core\backend;

use Yii;

abstract class Module extends \yii2portal\core\common\Module
{
    public function init()
    {
        parent::init();
        self::registerTranslations($this->id);
        $this->registerComponents();
    }


    public static function registerTranslations($module)
    {
        Yii::$app->i18n->translations["yii2portal/{$module}"] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => "@yii2portal/{$module}/backend/messages",
            'fileMap' => [
                "yii2portal/{$module}" => 'app.php'
            ],
        ];
    }

    public function registerComponents(){

        Yii::$app->set('subforms', [
            'class' => 'yii2portal\core\backend\components\Subforms',
        ]);
    }
}