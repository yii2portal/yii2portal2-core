<?php
namespace yii2portal\core\backend\models;

use yii2portal\core\backend\forms\SubformTrait;

class ActiveRecord extends \yii\db\ActiveRecord
{
    use SubformTrait;
}
