<?php
namespace yii2portal\core\common\widgets;

use yii\bootstrap\BootstrapWidgetTrait;

class Widget extends \yii2portal\core\common\components\Component
{
    use BootstrapWidgetTrait;

    /**
     * @var array the HTML attributes for the widget container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

}
