<?php

namespace yii2portal\core\common\components;

use Yii;

abstract class Component extends \yii\base\Widget{
    public function getViewPath()
    {
        $class = new \ReflectionClass($this);
        list(, $module) = explode('\\', $class->getNamespaceName());

        $viewPath = implode(DIRECTORY_SEPARATOR,[
            Yii::$app->getModule($module)->viewPath,
            "widgets",
            $class->getShortName()
        ]);

        if(is_dir($viewPath)){
            return $viewPath;
        }else{
            return parent::getViewPath();
        }
    }
}